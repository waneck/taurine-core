package taurine.tests;
import utest.Runner;
import utest.ui.Report;

/**
 * ...
 * @author waneck
 */
class Test
{

	static function main()
	{
		var runner = new Runner();

		runner.addCase(new taurine.tests.ds.LstTests());
		runner.addCase(new taurine.tests.UInt8Tests());
		// runner.addCase(new taurine.tests.mem.RawMemTests());
#if js
		// runner.addCase(new taurine.tests.mem.RawMemTests.RawMemTestsBackwards());
		// runner.addCase(new taurine.tests.mem.RawMemTests.RawMemTestsArray());
#end
		var report = new utest.ui.text.PrintReport(runner);
		runner.run();

#if sys
		// Sys.exit(report.allOk() ? 0 : 1);
#end
	}

}
